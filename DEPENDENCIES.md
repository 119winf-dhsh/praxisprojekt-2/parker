# Dependencies

I want to note down some information about some of the dependencies defined in `package.json`.

## mongoose

`mongoose` is fixed at `v5.11.15` because of an issue: [Warning: Accessing non-existent property 'MongoError' of module exports inside circular dependency · Issue #9900 · Automattic/mongoose · GitHub](https://github.com/Automattic/mongoose/issues/9900)
