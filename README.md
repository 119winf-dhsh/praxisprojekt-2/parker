# parker

> Prototype project created to showcase the usage of alternative technologies.

## Requirements

- Node.js >= 14

- A MongoDB instance

- A Redis instance

- A modern webbrowser (when using `HTTPS` then support for `HTTP/2` is required)

## Installation

##### 1. Clone the repository

##### 2. Install dependencies

```shell
$ npm install --only=production
```

##### 3. Setup configuration

```shell
$ cp .env.example .env
$ nano .env
```

Since all the configuration is stored in the environment, copy the example file and change the values to suit your needs.

##### 4. Run

```shell
$ node src/boot.js
```

## License

[GNU GPLv3](LICENSE)

```
parker - parking management software prototype
Copyright (C) 2021  Axel Rindle

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <https://www.gnu.org/licenses/>.
```
