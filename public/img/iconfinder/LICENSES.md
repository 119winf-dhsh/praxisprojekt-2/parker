# Licenses

Below is a list of 3rd party files alongside with the copyright and license notice.

## Files

### parking_sign.svg

Found on [Iconfinder](https://www.iconfinder.com/icons/5452502/automobile_car_parking_sign_signaling_signs_icon)

This icon has been releases under the [CC BY 3.0](https://creativecommons.org/licenses/by/3.0/) license and copyright goes to [Manthana Chaiwong on Iconfinder](https://www.iconfinder.com/Manthana)
