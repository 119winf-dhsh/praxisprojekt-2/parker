#!/usr/bin/env bash

KEY=$(curl --silent https://random-word-api.herokuapp.com/word | jq -r .[0] | sha256sum -z | sed s/\ \ -//g)
sed -i.bak "s/^APP_KEY=.*/APP_KEY=$KEY/" .env
