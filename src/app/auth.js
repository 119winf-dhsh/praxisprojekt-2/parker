// Require modules
const { nanoid } = require('nanoid');
const passport = require('passport');
const LocalStrategy = require('passport-local').Strategy;
const RememberMeStrategy = require('passport-remember-me').Strategy;
const { Logger } = require('../init/logger');
const crypt = require('../utils/crypt');

const AVAILABLE_STRATEGIES = module.exports.AVAILABLE_STRATEGIES = [
    'local'
];

/**
 * @param {import('express').Application} app
 */
module.exports.init = app => {
    /** @type {import('awilix').AwilixContainer} */
    const container = app.get('container');

    /** @type {Logger} */
    const logger = container.resolve('createLogger')('auth');

    /** @type {import('mongoose').Model} */
    const User = container.resolve('mongo').getModel('User');

    /** @type {import('mongoose').Model} */
    const RememberMeToken = container.resolve('mongo').getModel('RememberMeToken');

    // configure strategies
    passport.use(new LocalStrategy(
        (username, password, done) => {
            User.findOne({ username: username },
                /**
                 * @param {Error|null} err
                 * @param {{ username: string, email: string, password: string }} user
                 */
                (err, user) => {
                    if (err || user) logger.debug(err || user);
                    if (err) { return done(err); }
                    if (! user) { return done(null, false, { message: 'Unknown username!' }); }

                    crypt.compare(password, user.password, (err, same) => {
                        if (err) { return done(err); }
                        if (! same) {
                            return done(null, false, { message: 'Incorrect password!' });
                        } else {
                            return done(null, user);
                        }
                    });
                });
        }
    ));
    passport.use(new RememberMeStrategy(
        (token, done) => {
            RememberMeToken.find({ token }, (err, doc) => {
                if (err) done(err);
                else if (! doc) done(null, false);
                else done(null, doc.user);
            });
        },
        (user, done) => {
            const token = nanoid();
            RememberMeToken.create({ token, user }, (err, _rememberMeToken) => {
                if (err) done(err);
                else done(null, token);
            });
        }
    ));

    // serialization
    passport.serializeUser((user, done) => {
        done(null, user._id);
    });
    passport.deserializeUser((id, done) => {
        User.findById(id, (err, user) => {
            done(err, user);
        });
    });

    // install passport
    app.use(passport.initialize());
    app.use(passport.session());
    app.use(passport.authenticate('remember-me'));
};

/**
 * @param {import('express').Application} app
 */
module.exports.routes = app => {
    app.get('/login', (req, res) => {
        if (req.isAuthenticated()) {
            res.redirect('/');
        } else {
            res.redirect('/login/local');
        }
    });
    app.get('/login/:provider', (req, res) => {
        const provider = req.params.provider || 'local';
        res.render(`auth/login.${provider}.njk`);
    });
    app.post('/login/local',
        (req, res, next) => {
            req.session.flash.old = { username: req.body.username, rememberMe: req.body.rememberMe };
            next();
        },
        passport.authenticate('local', {
            failureRedirect: '/login',
            failureFlash: true
        }),
        function(req, res, next) {
            // issue a remember me cookie if the option was checked
            if (!req.body.remember_me) { return next(); }

            /** @type {import('awilix').AwilixContainer} */
            const container = app.get('container');

            /** @type {import('mongoose').Model} */
            const RememberMeToken = container.resolve('mongo').getModel('RememberMeToken');

            const token = nanoid();
            RememberMeToken.create({
                token,
                user: req.user
            }, (err, rememberMeToken) => {
                if (err) return next(err);
                else {
                    res.cookie('remember_me', token, { path: '/', httpOnly: true, maxAge: 604800000 }); // 7 days
                    return next();
                }
            });
        },
        function(req, res) {
            res.redirect('/');
        }
    );
    app.get('/logout', (req, res) => {
        req.logout();
        res.redirect('/');
    });
};
