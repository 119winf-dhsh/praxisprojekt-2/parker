// Require modules
const { Model } = require('mongoose');
const testObjectId = require('../../utils/test-object-id');
const Controller = require('./Controller');

module.exports = class AdminController extends Controller {

    /**
     * @param {import('express').Request} req
     * @param {import('express').Response} res
     */
    home(req, res) {
        res.redirect('admin/users');
    }

    /**
     * Shows a page with a paginated list of all users.
     *
     * @param {import('express').Request} req
     * @param {import('express').Response} res
     */
    showPageUsers(req, res) {
        /** @type {Model} */
        const User = this._container.resolve('mongo').getModel('User');
        const finder = { username: { $regex: req.query.username || '' } };
        const renderOpts = { username: req.query.username };
        this._paginate(req, res, User, 10, finder, 'admin/users', 'admin/page.users.njk', renderOpts);
    }

    /**
     * Shows a form with detailed user information.
     *
     * @param {import('express').Request} req
     * @param {import('express').Response} res
     */
    async showFormUser(req, res) {
        /** @type {Model} */
        const User = this._container.resolve('mongo').getModel('User');
        this._showModelForm(req, res, User, 'admin/form/user.details.njk');
    }

};
