// Require modules
const { Model } = require('mongoose');
const Controller = require('./Controller');
const { testObjectId } = require('../../utils');

module.exports = class ParkingController extends Controller {

    /**
     * @param {import('express').Request} req
     * @param {import('express').Response} res
     */
    async listAreas(req, res) {
        /** @type {Model} */
        const ParkingArea = this._container.resolve('mongo').getModel('ParkingArea');

        try {
            const areas = await ParkingArea
                .find()
                .select('name location')
                .exec();
            res.send(areas);
        } catch (error) {
            this._logger.error(error);
            res.status(500).send({ error});
        }
    }

    /**
     * @param {import('express').Request} req
     * @param {import('express').Response} res
     */
    async listSeats(req, res) {
        /** @type {Model} */
        const ParkingArea = this._container.resolve('mongo').getModel('ParkingArea');

        /** @type {Model} */
        const Seat = this._container.resolve('mongo').getModel('Seat');

        const areaId = req.params.areaId;
        if (! areaId && ! testObjectId(areaId)) {
            return res.send({ error: `No parking area found with ID "${areaId}" !` })
        }

        try {
            const area = await ParkingArea
                .findById(areaId)
                .select('seats')
                .exec();
            res.send(area.seats);
        } catch (error) {
            this._logger.error(error);
            res.status(500).send({ error });
        }
    }

}
