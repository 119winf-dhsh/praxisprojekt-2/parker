// Require modules
const { Model } = require('mongoose');
const { Logger } = require('../../init/logger');
const { testObjectId, url } = require('../../utils');

module.exports = class Controller {

    /**
     * Constructs a new controller instance.
     *
     * @param {import('express').Application} app
     */
    constructor(app) {
        /** @type {import('express').Application} */
        this._app = app;

        /** @type {import('awilix').AwilixContainer} */
        this._container = app.get('container');

        /** @type {Logger} */
        this._logger = this._container.resolve('createLogger')(this._name);
    }

    get _name() {
        return this.constructor.name;
    }

    /**
     * Binds a local function to this controller instance.
     *
     * @param {string} fun The name of the function to bind.
     * @returns {import('express').RequestHandler} A bound function.
     */
    bound(fun) {
        return this[fun].bind(this);
    }

    /**
     * @param {import('express').Request} req The request object.
     * @param {import('express').Response} res The response object.
     * @param {import('mongoose').Model | Function} model The model to query.
     * @param {string} view The view to render.
     */
    async _showModelForm(req, res, model, view) {
        const id = req.params.id;

        try {
            if (! testObjectId(id)) {
                throw new Error(`ID "${id}" is invalid!`);
            }

            let result;
            if (typeof model === 'function') {
                result = await model(id);
            } else {
                result = await model.findById(id).exec();
            }
            if (! result) {
                throw new Error(`No ${model.name} found with ID "${id}" !`);
            }

            res.render(view, {
                model: result
            });
        } catch (error) {
            this._logger.error(error);
            res.render(view, { error });
        }
    }

    /**
     * @param {import('express').Request} req The request object.
     * @param {import('express').Response} res The response object.
     * @param {Model} model The model to be queried.
     * @param {number} perPage How many entries to show per page.
     * @param {object} finder A selector to match only specific documents. An empty object includes all documents.
     * @param {string} path The url path accessed.
     * @param {string} view The view to render.
     * @param {object} customRenderOpts Custom data to pass to the view.
     */
    async _paginate(req, res, model, perPage, finder, path, view, customRenderOpts = {}) {
        /** @type {number} */
        const page = parseInt(req.query.page) || 1;
        // @ts-ignore
        if (page < 1 || isNaN(page)) {
            return res.redirect( url(path) );
        }

        this._logger.debug(`Skipping ${(page - 1) * perPage}`);

        try {
            const count = await model.countDocuments().exec();
            const foundModels = await model
                .find( finder )
                .skip( (page - 1) * perPage )
                .limit(perPage)
                .exec();
            const pages = Math.ceil(count / perPage);

            this._logger.debug(`Found ${foundModels.length} / ${count} total models.`);

            if (page > pages) {
                return res.redirect( url(path, { page: pages }) );
            }

            const renderOpts = Object.assign(customRenderOpts, {
                pages: pages,
                pageNum: page,
                models: foundModels
            });
            res.render(view, renderOpts);
        } catch (error) {
            this._logger.error(error);
            res.render(view, { error });
        }
    }
};
