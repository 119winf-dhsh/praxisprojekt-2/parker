// Require modules
const { Model } = require('mongoose');
const Controller = require('./Controller');
const RedisManager = require('../../init/redis');
const { dependencies } = require('../../../package.json');

module.exports = class HomeController extends Controller {

    /**
     * @param {import("express").Application} app
     */
    constructor(app) {
        super(app);

        /** @type {RedisManager} */
        this._redis = this._container.resolve('redis');
    }

    /**
     * @param {import('express').Request} _req
     * @param {import('express').Response} res
     */
    async home(_req, res) {
        /** @type {Model} */
        const ParkingArea = this._container.resolve('mongo').getModel('ParkingArea');

        const areas = await ParkingArea
            .find()
            .select('name location')
            .exec();

        res.render('index.njk', {
            areas
        });
    }

    /**
     * @param {import('express').Request} _req
     * @param {import('express').Response} res
     */
    async about(_req, res) {
        let deps = null;

        try {
            deps = await new Promise((resolve, reject) => {
                this._redis.client.get('dependencies', (err, reply) => {
                    if (err) reject(err);
                    else resolve(JSON.parse(reply));
                });
            });

            if (deps === null) {
                deps = Object.keys(dependencies);
                await new Promise((resolve, reject) => {
                    const ttl = 60 * 60 * 24 * 7;
                    this._redis.client.setex('dependencies', ttl, JSON.stringify(deps), (err, reply) => {
                        if (err) reject(err);
                        else resolve();
                    });
                });
            }
        } catch (error) {
            this._logger.error(error);
            deps = error.toString();
        }

        res.render('about.njk', {
            dependencies: deps
        });
    }

};
