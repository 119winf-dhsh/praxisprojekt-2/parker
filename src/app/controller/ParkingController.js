// Require modules
const { Model, Document } = require('mongoose');
const url = require('../../utils/url');
const Controller = require('./Controller');

module.exports = class ParkingController extends Controller {

    /**
     * @param {import('express').Request} _req
     * @param {import('express').Response} res
     */
    index(_req, res) {
        return res.redirect( url('manage/areas') );
    }

    /**
     * @param {import('express').Request} _req
     * @param {import('express').Response} res
     */
    indexCreate(_req, res) {
        return res.redirect( url('manage/create/area') );
    }

    /**
     * @param {import('express').Request} req
     * @param {import('express').Response} res
     */
    showAreaManagement(req, res) {
        /** @type {Model} */
        const ParkingArea = this._container.resolve('mongo').getModel('ParkingArea');
        const finder = { name: { $regex: req.query.name || '' } };
        const renderOpts = { name: req.query.name };
        this._paginate(req, res, ParkingArea, 10, finder, 'manage/areas', 'seats/page.areas.njk', renderOpts);
    }

    /**
     * @param {import('express').Request} _req
     * @param {import('express').Response} res
     */
    showFormAreaCreation(_req, res) {
        res.render('seats/form/create.area.njk');
    }

    /**
     * Shows a form with area details that can be edited.
     *
     * @param {import('express').Request} req
     * @param {import('express').Response} res
     */
    showFormArea(req, res) {
        /** @type {Model} */
        const ParkingArea = this._container.resolve('mongo').getModel('ParkingArea');
        const query = async id => {
            return await ParkingArea
                .findById(id)
                .populate('seats.holder')
                .exec();
        };
        this._showModelForm(req, res, query, 'seats/form/edit.area.njk');
    }

    /**
     * @param {import('express').Request} req
     * @param {import('express').Response} res
     */
    async createArea(req, res) {
        /** @type {Model} */
        const ParkingArea = this._container.resolve('mongo').getModel('ParkingArea');

        req.session.flash.old = req.body;

        if (! req.body.name) {
            req.flash('error', 'Name required!');
            return res.redirect('/manage/create/area');
        }

        this._logger.debug(await ParkingArea.findOne({ name: req.body.name }).exec());
        if (await ParkingArea.findOne({ name: req.body.name }).exec() !== null) {
            req.flash('error', `An area with the name "${req.body.name}" does already exist!`);
            return res.redirect('/manage/create/area');
        }

        try {
            /** @type {Document} */
            const newArea = new ParkingArea(req.body);
            await newArea.save();
        } catch (error) {
            this._logger.error(error);
            req.flash('error', error.toString());
            return res.redirect('/manage/create/area');
        }

        req.session.flash.old = null;
        res.redirect('/manage/areas');
    }

};
