// Require modules
const { Environment } = require('nunjucks');
const { isDebug, hasAccess, url } = require('../utils');
const { description, version } = require('../../package.json');

/**
 * @type {import('express').RequestHandler}
 */
const middleware = (req, res, next) => {
    res.locals = Object.assign({}, res.locals, {

        app: { description, version },

        env: process.env,
        isAuthenticated: req.isAuthenticated(),
        user: req.user,

        isDebug: isDebug,
        csrfToken: req.csrfToken(),

        /**
         * Inserts a hidden input field used for CSRF validation.
         */
        csrf: function () {
            return `<input type="hidden" name="_csrf" value="${req.csrfToken()}">`;
        }

    });

    next();
};

/**
 * @param {import('express').Application} app
 * @param {Environment} nunjucks
 */
module.exports = (app, nunjucks) => {
    app.use(middleware);

    nunjucks.addGlobal('hasAccess',
        /**
         * @param {string} level
         */
        function (level) {
            return hasAccess(this.ctx, level);
        }
    );

    nunjucks.addGlobal('url', url);

    if (isDebug) {
        nunjucks.addGlobal('stringify', JSON.stringify);
    }
};
