// Require modules
const csurf = require('csurf');
const env = require('../../utils/env');

module.exports = csurf({
    secure: env.text('APP_PROTOCOL', 'http') === 'https',
    sameSite: true,
    httpOnly: true
});
