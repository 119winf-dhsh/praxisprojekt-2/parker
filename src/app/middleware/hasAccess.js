const hasAccess = require('../../utils/has-access');

/**
 * @param {string} accessLevel
 */
module.exports = (accessLevel, redirectFailure = '/') => {
    /**
     * @param {import('express').Request} req
     * @param {import('express').Response} res
     * @param {import('express').NextFunction} next
     */
    const func = (req, res, next) => {
        // @ts-ignore
        if (hasAccess(req, accessLevel)) {
            return next();
        }

        return res.redirect(redirectFailure);
    };

    return func;
};
