// Require modules
const { Router } = require('express');
const hasAccess = require('../middleware/hasAccess');
const AdminController = require('../controller/AdminController');

/**
 * @param {import('express').Application} app
 * @returns {Router}
 */
module.exports = app => {
    const router = Router();
    const controller = new AdminController(app);

    router.use(hasAccess('admin'));

    router.get('/', (req, res) => {
        res.redirect('admin/users');
    });

    router.get('/users', controller.bound('showPageUsers'));
    router.get('/users/:id', controller.bound('showFormUser'));

    return router;
};
