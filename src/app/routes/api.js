// Require modules
const { Router } = require('express');
const ParkingController = require('../controller/ApiController');
const hasAccess = require('../middleware/hasAccess');

/**
 * @param {import('express').Application} app
 * @returns {Router}
 */
module.exports = app => {
    const router = Router();
    const parkingController = new ParkingController(app);

    router.use(hasAccess('write'));

    router.get('/areas', parkingController.bound('listAreas'));
    router.get('/seats/:areaId?', parkingController.bound('listSeats'));

    return router;
};
