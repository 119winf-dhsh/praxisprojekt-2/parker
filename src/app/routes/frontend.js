// Require modules
const { Router } = require('express');
const hasAccess = require('../middleware/hasAccess');
const HomeController = require('../controller/HomeController');
const ParkingController = require('../controller/ParkingController');

/**
 * @param {import('express').Application} app
 * @returns {Router}
 */
module.exports = app => {
    const router = Router();
    const homeController = new HomeController(app);
    const parkingController = new ParkingController(app);

    const checkWriteAccess = hasAccess('write');

    router.get('/', homeController.bound('home'));
    router.get('/about', homeController.bound('about'));

    router.get('/manage', checkWriteAccess, parkingController.bound('index'));
    router.get('/manage/areas', checkWriteAccess, parkingController.bound('showAreaManagement'));
    router.get('/manage/areas/:id', checkWriteAccess, parkingController.bound('showFormArea'));

    router.get('/manage/create', checkWriteAccess, parkingController.bound('indexCreate'));
    router.get('/manage/create/area', checkWriteAccess, parkingController.bound('showFormAreaCreation'));
    router.post('/manage/create/area', checkWriteAccess, parkingController.bound('createArea'));

    return router;
};
