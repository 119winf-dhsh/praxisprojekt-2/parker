// Require modules
const { Router } = require('express');

/**
 * @param {import('express').Application} app
 */
module.exports = app => {

    app.use('/', require('./frontend')(app));
    app.use('/admin', require('./admin')(app));
    app.use('/api', require('./api')(app));

};
