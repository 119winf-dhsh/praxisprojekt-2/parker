// Require modules
const EventEmitter = require('events');
const fs = require('fs');
const path = require('path');
const awilix = require('awilix');
const dotenv = require('dotenv');
const dotenvExpand = require('dotenv-expand');

const ROOT_DIRECTORY = path.dirname(__dirname);

// Load configuration
const environment = dotenv.config();
dotenvExpand(environment);

const fail = require('./utils/fail');
const isDebug = require('./utils/is-debug');
const { StopWatch } = require('stopwatch-node');

// # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # #
// #
// # Logging creation
// #
const logger = require('./init/logger')('main');
const header = fs.readFileSync(path.join(ROOT_DIRECTORY, 'resources/log_head.txt')).toString();
console.log(header);
logger.info('Initializing...');

const watch = new StopWatch('boot');


// # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # #
// #
// # Check required environment variables
// #
if (! process.env.APP_KEY) {
    fail('Required environment variable APP_KEY not set!');
}
if (! [ 'production', 'debug' ].includes( process.env.NODE_ENV )) {
    fail(`Invalid environment "${process.env.NODE_ENV}"! Must be either "production" or "debug".`);
}


// # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # #
// #
// # Container initialization & service registration
// #
if (isDebug) {
    watch.start('container init');
}
const container = awilix.createContainer({
    injectionMode: awilix.InjectionMode.PROXY
});

container.register('ROOT_DIRECTORY', awilix.asValue(ROOT_DIRECTORY));

// Init services
const disposer = service => service.dispose();
const asServiceClass = clazz => awilix.asClass(clazz).singleton().disposer(disposer);

container.register('eventbus', awilix.asValue( new EventEmitter() ));
container.register('createLogger', awilix.asValue( require('./init/logger') ));
container.register('mongo', asServiceClass( require('./init/mongodb') ));
container.register('redis', asServiceClass( require('./init/redis') ));

logger.info('Container initialized.');
watch.stop();


// # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # #
// #
// # Initialize services & start the server
// #
(async () => {
    try {
        watch.start('service init');
        await container.resolve('mongo').init();
        await container.resolve('redis').init();
        watch.stop();

        watch.start('server init');
        await require('./server')(container);
        watch.stop();

        // print time measurements
        if (isDebug) {
            console.log();
            watch.prettyPrint();
            console.log();
        }
    } catch (err) {
        fail(err);
    }
})();
