// Require modules
const fs = require('fs').promises;
const http = require('http');
const https = require('https');
const spdy = require('spdy');
const env = require('./utils/env');

const protocol = env.get('APP_PROTOCOL', 'http', value => [ 'http', 'https' ].includes(value));

/**
 * @param {import('express').Application} app
 */
module.exports = async (app) => {

    if (protocol === 'http') {
        return http.createServer(app);
    }
    else if (protocol === 'https') {
        const opts = {
            cert: await fs.readFile( env.text('HTTPS_CERT_FILE') ),
            key: await fs.readFile( env.text('HTTPS_KEY_FILE') )
        };
        // const httpsServer = https.createServer(opts, app);
        const httpsServer = spdy.createServer(opts, app);
        return httpsServer;
    }
    else {
        throw new Error(`Invalid protocol "${protocol}"!`);
    }

};
