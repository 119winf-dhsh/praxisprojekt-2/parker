// Require modules
const WritableStream = require('stream').Writable;
const chalk = require('chalk');
const dayjs = require('dayjs');
const { env, isDebug } = require('../utils');

/**
 * A simple utility class for writing formatted logging messages.
 */
class Logger {

    /**
     * @param {string} tag
     */
    constructor(tag) {
        this._locale = env.text('APP_LOCALE', 'en');
        this._tag = tag;
        this._levels = {
          debug: {
            formatter: chalk.rgb(128, 0, 255).bold,
            writer: msg => process.stdout.write(msg)
          },
          info: {
            formatter: chalk.blue,
            writer: msg => process.stdout.write(msg)
          },
          warn: {
            formatter: chalk.yellow,
            writer: msg => process.stderr.write(msg)
          },
          error: {
            formatter: chalk.red,
            writer: msg => process.stderr.write(msg)
          },
          fatal: {
            formatter: chalk.red.bold,
            writer: msg => process.stderr.write(msg)
          },
        };
        this._levelsKeys = Object.keys(this._levels);
        this._level = isDebug ? 'debug' : env.get('LOG_LEVEL', 'info', value => this._levelsKeys.includes(value));

        dayjs.locale(this._locale);

        WritableStream.prototype._write = (chunk, encoding, callback) => {
            this._log('info', chunk);
            callback();
        };
        this.pipe = new WritableStream();
    }

    /**
     * Checks whether the given log level will be logged.
     *
     * @param {string} level
     * @returns {boolean}
     */
    _canBeLogged(level) {
        const pos = this._levelsKeys.indexOf(this._level);
        const levels = this._levelsKeys.slice(pos, this._levels.length);
        return levels.includes(level);
    }

    /**
     * Converts a message into a string if it's not already.
     *
     * @param {any} message
     * @returns {string}
     */
    _stringify(message) {
      if (Buffer.isBuffer(message)) {
        return message.toString();
      }
      else if (message === undefined) {
        return 'undefined';
      }
      else if (message === null) {
        return 'null';
      }
      else if (message instanceof Error) {
        return message.message;
      }
      else if (typeof message === 'object') {
        return JSON.stringify(message);
      }

      return message.toString();
    }

    /**
     * Common log function. Formats the message and logs it.
     *
     * @param {string} level
     * @param {any} message
     */
    _log(level, message) {
        const localPos = this._levelsKeys.indexOf(level);
        if (localPos === -1) throw new Error(`Invalid logging level "${level}"!`);

        if (! this._canBeLogged(level)) return;

        const stringifiedMessage = this._stringify(message);
        const theLevel = this._levels[level];
        const timestamp = dayjs().format( env.text('LOG_TIMESTAMP_FORMAT', 'DD.MM.YYYY HH:mm:ss') );
        const formattedLevel = theLevel.formatter(level);
        const formattedTag = chalk.underline(this._tag);
        let format = `${chalk.dim(timestamp)} ${chalk.dim('[')}${formattedLevel}${chalk.dim(']')} ${formattedTag} > ${stringifiedMessage}`;
        if (! format.endsWith('\n')) {
            format += '\n';
        }
        theLevel.writer(format);

        // print any metadata
        if (message instanceof Error && message.stack) {
          theLevel.writer(`${message.stack}\n`);
        }
    }

    /**
     * Logs a DEBUG message.
     *
     * @param {any} message
     */
    debug(message) {
        this._log('debug', message);
    }

    /**
     * Logs an INFO message.
     *
     * @param {any} message
     */
    info(message) {
        this._log('info', message);
    }

    /**
     * Logs a WARN message.
     *
     * @param {any} message
     */
    warn(message) {
        this._log('warn', message);
    }

    /**
     * Logs an ERROR message.
     *
     * @param {any} message
     */
    error(message) {
        this._log('error', message);
    }

    /**
     * Logs an FATAL message.
     *
     * @param {any} message
     */
    fatal(message) {
        this._log('fatal', message);
    }
}

/**
 * Creates a new Logger instance.
 *
 * @param {string} tag The logging tag.
 * @return {Logger} A new Logger instance.
 */
module.exports = tag => new Logger(tag);
module.exports.Logger = Logger;
