// Require modules
const EventEmitter = require('events');
const fs = require('fs').promises;
const path = require('path');
const chalk = require('chalk');
const mongoose = require('mongoose');
const { crypt, env, isDebug } = require('../utils');
const { Logger } = require('./logger');

module.exports = class MongoDB {

    constructor({ createLogger, eventbus }) {
        /** @type {Logger} */
        this._logger = createLogger('mongodb');

        /** @type {EventEmitter} */
        this._eventbus = eventbus;
    }

    async _loadModels() {
        const directory = path.join(__dirname, "..", "model");
        const list = (await fs.readdir(directory)).map(el => el.replace('.js', ''));

        for (const model of list) {
            mongoose.model(model, require(path.join(directory, model)));
        }

        this._logger.debug(`Loaded ${list.length} model(s).`);
    }

    async _connect() {
        const uri = env.text('MONGO_URI');
        const opts = {
            auth: {
                authdb: "admin",
                user: env.text('MONGO_USER'),
                password: env.text('MONGO_PASS')
            },
            autoIndex: isDebug,
            useCreateIndex: true, // https://mongoosejs.com/docs/deprecations.html#ensureindex
            useNewUrlParser: true,
            useUnifiedTopology: true
        };
        await mongoose.connect(uri, opts);

        this._logger.info('Successfully connected.');

        mongoose.connection.on('error', err => {
            this._logger.error(err.message);
        });
        mongoose.connection.on('reconnected', () => {
            this._logger.info('Successfully reconnected to the database.');
        });
        mongoose.connection.on('reconnectFailed', () => {
            this._logger.error('Failed to reconnect to the database!');
            this._eventbus.emit('shutdown');
        });
        mongoose.connection.on('disconnected', err => {
            this._logger.warn('Connection has been lost!');
        });

        if (isDebug) {
            mongoose.set('debug', (collection, method, ...args) => {
                const argString = args.map(el => JSON.stringify(el)).join(', ');
                this._logger.debug(`Query fired: ${chalk.underline(`${collection}.${method}(${argString})`)}`);
            });
        }
    }

    async _seedDatabase() {
        let seeded = false;
        const User = mongoose.model('User');
        if (await User.estimatedDocumentCount() === 0) { // another mechanism should be used here
            await User.create({
                username: 'admin',
                email: 'admin@localhost',
                password: await crypt.hash('admin'),
                role: 'admin'
            });

            seeded = true;
        }

        if (seeded) this._logger.info('Database has been seeded.');
    }

    async init() {
        await this._loadModels();
        await this._connect();
        await this._seedDatabase();
    }

    async dispose() {
        await mongoose.disconnect();
        this._logger.info('Disposed.');
    }

    /**
     * Retrieves a Model based on it's name.
     *
     * @param {string} name The model name.
     * @returns {mongoose.Model|null}
     */
    getModel(name) {
        try {
            return mongoose.model(name);
        } catch (error) {
            if (error instanceof mongoose.Error.MissingSchemaError) {
                return null;
            } else {
                throw error;
            }
        }
    }

}
