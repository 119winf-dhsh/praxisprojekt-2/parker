// Require modules
const EventEmitter = require('events');
const redis = require("redis");
const chalk = require('chalk');
const { env, isDebug } = require('../utils');
const { Logger } = require('./logger');

const RETRY_MAX_ATTEMPTS = 10;
const retryStrategy = function(options) {
  // if (options.error && options.error.code === "ECONNREFUSED") {
  //   // End reconnecting on a specific error and flush all commands with
  //   // a individual error
  //   return new Error("The server refused the connection");
  // }
  if (options.total_retry_time > 1000 * 60 * 60) {
    // End reconnecting after a specific timeout and flush all commands
    // with a individual error
    return new Error("Retry time exhausted");
  }
  if (options.attempt > RETRY_MAX_ATTEMPTS) {
    // End reconnecting with built in error
    return undefined;
  }
  // reconnect after
  return Math.min(options.attempt * 100, 3000);
};

module.exports = class RedisManager {

  constructor({ createLogger, eventbus }) {
    /** @type {Logger} */
    this._logger = createLogger('redis');
    this._loadConfig();

    /** @type {EventEmitter} */
    this._eventbus = eventbus;
  }

  _loadConfig() {
    this._config = {
        host: env.text('REDIS_HOST', 'localhost'),
        port: env.int('REDIS_PORT', 6379),
        db: env.int('REDIS_DB', 0),
        prefix: env.text('REDIS_PREFIX', null),
        retry_strategy: retryStrategy
    };
    if (env.has('REDIS_USER')) {
        this._config.user = env.text('REDIS_USER');
    }
    if (env.has('REDIS_PASS')) {
        this._config.pass = env.text('REDIS_PASS');
    }

    // redis.debug_mode = isDebug;
  }

  async init() {
    this.client = redis.createClient(this._config);

    await new Promise((resolve, reject) => {
      this.client.once('error', err => {
        reject(err);
      });
      this.client.once('ready', () => {
        resolve();
      });
    });

    this.client.on('error', err => {
      this._logger.error(err);
    });
    this.client.on('reconnecting', ({ delay, attempt }) => {
      this._logger.info(`Connection lost. Trying to reconnect after ${chalk.underline(delay + "ms")}... (Attempt #${chalk.underline(attempt)})`);
      if (attempt >= RETRY_MAX_ATTEMPTS) {
        this._logger.error('Failed to reconnect to the database!');
        this._eventbus.emit('shutdown');
      }
    });
    this.client.on('ready', () => {
      this._logger.info('Connected to redis server.');
    });
    this.client.on('warning', err => {
      this._logger.warn(err);
    });

    if (isDebug) {
      this.client.monitor((err, res) => {
        if (err) this._logger.error(err.toString());
        else this._logger.debug('Monitoring enabled.');
      });
      this.client.on('monitor', (_time, args, rawReply) => {
        this._logger.debug(`Command "${chalk.underline(args)}" replied with "${chalk.underline(rawReply)}"`);
      });
    }
  }

  /**
   * Indicates whether a connection is currently established.
   */
  get isConnected() {
    return this.client.connected;
  }

  /**
   * Executes the given redis command on the current connection.
   *
   * @param {string} cmd The command to execute.
   * @param {...any} args An arguments to pass to the command. The last argument must be the callback.
   */
  run(cmd, ...args) {
    this.client[cmd](...args);
  }

  dispose() {
    return new Promise((resolve, reject) => {
      this.client.quit(err => {
        if (err) reject(err);
        else {
          this._logger.info('Disposed.');
          resolve();
        }
      })
    });
  }

}
