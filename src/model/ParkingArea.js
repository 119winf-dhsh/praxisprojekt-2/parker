// Require modules
const { Schema, Types } = require('mongoose');

const SchemaSeat = new Schema({
    name: String,
    holder: {
        type: Types.ObjectId,
        ref: 'User',
        default: null
    }
});

const SchemaParkingArea = new Schema({
    name: {
        type: String,
        required: true
    },
    location: {
        name: String,
        coordinates: {
            type: [String],
            required: function () {
                return this.location.name !== null && this.location.name !== undefined;
            }
        }
    },
    seats: [SchemaSeat]
});

module.exports = SchemaParkingArea;
