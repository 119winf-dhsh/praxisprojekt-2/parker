// Require modules
const { Schema } = require('mongoose');
const User = require('./User');

const schema = new Schema({
    token: {
        type: String,
        required: true
    },
    user: {
        type: User,
        required: true
    }
});

module.exports = schema;
