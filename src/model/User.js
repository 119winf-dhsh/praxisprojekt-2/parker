// Require modules
const { Schema } = require('mongoose');
const role = require('mongoose-role');
const { AVAILABLE_STRATEGIES } = require('../app/auth');

const schema = new Schema({
    username: {
        type: String,
        required: true,
        index: true,
        unique: true
    },
    email: String,
    password: String,
    meta: {
        backend: {
            type: String,
            enum: AVAILABLE_STRATEGIES,
            default: AVAILABLE_STRATEGIES[0]
        }
    }
});

const roles = [ 'user', 'manager', 'admin' ];
schema.plugin(role, {
    roles: roles,
    accessLevels: {
        read: roles,
        write: roles.slice(1, 3),
        admin: roles.slice(2, 3)
    }
});

schema.query.byUsername = function (name) {
    return this.where({ username: new RegExp(name, 'i') })
};

schema.methods.isEditable = function () {
    // @ts-ignore
    return this.backend === AVAILABLE_STRATEGIES[0];
};

module.exports = schema;
module.exports.roles = roles;
