// Require modules
const EventEmitter = require('events');
const path = require('path');
const express = require('express');
const { createTerminus } = require('@godaddy/terminus');
const helmet = require('helmet');
const compression = require('compression');
const morgan = require('morgan');
const bodyParser = require('body-parser');
const cookieParser = require('cookie-parser');
const nunjucks = require('nunjucks');
const session = require('express-session')
const RedisStore = require('connect-redis')(session);
const favicon = require('express-favicon');
const flash = require('express-flash');
const back = require('express-back');
const { env, isDebug } = require('./utils');
const createServer = require('./create-server');
const { Logger } = require('./init/logger');

/**
 * @param {import('awilix').AwilixContainer} container
 */
module.exports = async (container) => {

    /** @type {Logger} */
    const logger = container.resolve('createLogger')('server');

    const ROOT_DIRECTORY = container.resolve('ROOT_DIRECTORY');
    const app = express();

    const renderEnv = nunjucks.configure('resources/views', {
        noCache: isDebug,
        autoescape: true,
        express: app
    });

    // configure express
    app.disable('x-powered-by');
    app.set('view engine', 'nunjucks');
    app.set('views', 'resources/views');
    app.set('container', container);

    // install middleware
    app.use(helmet());
    app.use(compression());
    app.use(express.static(path.join( ROOT_DIRECTORY, 'public' )));
    app.use(morgan('short', {
        stream: logger.pipe
    }));
    app.use(bodyParser.json());
    app.use(bodyParser.urlencoded({ extended: true }));
    app.use(cookieParser( env.text('APP_KEY') ));
    app.use(favicon(path.join( ROOT_DIRECTORY, 'public/img/favicon.png' )));
    app.use(session({
        store: new RedisStore({ client: container.resolve('redis').client }),
        secret: env.text('APP_KEY'),
        resave: false,
        saveUninitialized: true
    }));
    app.use(flash());
    app.use(back());

    // install routes
    const auth = require('./app/auth');
    auth.init(app);
    app.use(require('./app/middleware/csrf'));
    require('./app/locals')(app, renderEnv);
    auth.routes(app);
    require('./app/routes')(app);

    // request error handler
    app.use(
        /**
         * @param {import('express').Request} req
         * @param {import('express').Response} res
         */
        (req, res) => {
            res.status(404);

            // respond with html page
            if (req.accepts('html')) {
                res.render('error/404.njk', { backUrl: req.prevPath });
            }

            // respond with json
            else if (req.accepts('json')) {
                res.send({ error: 'Not found' });
            }

            // default to plain text
            else {
                res.type('txt').send('Not found');
            }
        }
    );

    // listen on configured port
    const port = env.int('APP_PORT', 8080);
    const server = await createServer(app);
    server.listen(port, () => logger.info(`Server listening on port ${port}.`));

    // setup graceful shutdown
    createTerminus(server, {
        signals: [ 'SIGTERM', 'SIGINT' ],
        onSignal: () => {
            logger.info('Shutdown...');
            return Promise.all([
                container.dispose()
            ]);
        },
        // @ts-ignore
        onShutdown: () => {
            logger.info('Goodbye :)');
        }
    });

    // listen to shutdown events
    /** @type {EventEmitter} */
    const eventbus = container.resolve('eventbus');
    eventbus.once('shutdown', () => {
        process.emit('SIGTERM');
    });
};
