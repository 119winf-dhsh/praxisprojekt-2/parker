// Require modules
const bcrypt = require('bcrypt');
const env = require('./env');

const saltRounds = env.int('SECURITY_SALT_ROUNDS', 10);

/**
 * Creates a cryptographic hash for the given input.
 *
 * @param {string} input The input data.
 */
module.exports.hash = input => new Promise((resolve, reject) => {
    bcrypt.genSalt(saltRounds, (err1, salt) => {
        if (err1) reject (err1);
        else bcrypt.hash(input, salt, (err2, hash) => {
            if (err2) reject(err2);
            else resolve(hash);
        });
    });
});

/**
 * Compares a plain-text input with a given hash to check whether they are the same value.
 *
 * @param {string} input The data to check.
 * @param {string} hash The hash created with `hash()` to compare against.
 * @param {(err: Error, same: boolean) => void} callback A callback fired with the results. May be omitted to use promises instead.
 * @returns {Promise<boolean>}
 */
module.exports.compare = (input, hash, callback) => bcrypt.compare(input, hash, callback);
