// Require modules
const logger = require('../init/logger')('main');

/**
 * Logs an error and terminates the process.
 *
 * @param {string|Error} err
 */
module.exports = err => {
    logger.fatal(err);
    process.exit(-1);
};
