/**
 * Checks whether a user is authenticated and has access to the given level.
 *
 * @param {object} ctx An object which has the `user` property available.
 * @param {string} level The access level to check.
 */
module.exports = (ctx, level) => {
    return ctx.user && ctx.user.hasAccess(level);
};
