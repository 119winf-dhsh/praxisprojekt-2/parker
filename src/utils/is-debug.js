// Require modules
const env = require('./env');

module.exports = env.text('NODE_ENV', 'production') === 'debug';
