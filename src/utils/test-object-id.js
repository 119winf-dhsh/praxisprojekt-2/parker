/**
 * Tests a value to be a valid mongoose ObjectId.
 * @param {string} value
 * @returns {boolean}
 */
module.exports = value => {
    if (typeof value !== 'string') return false;
    return value.match(/^[0-9a-fA-F]{24}$/) !== null;
};
